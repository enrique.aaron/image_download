#!/bin/bash

# Get Serial Number
serial_number=$(tr -d '\0' < /proc/device-tree/serial-number)

# Get WiFi MAC Address
wifi_mac=$(cat /sys/class/net/wlan0/address)

# Get Ethernet MAC Address
eth_mac=$(cat /sys/class/net/eth0/address)

# Get Tegra release
tegra_rel=$(cat /etc/nv_tegra_release)

# Get Timestamp
timestamp=$(date)

mv care.ai.id.json .care.ai.id.json

# Updating the values in the .care.ai.id.json file
jq --arg serial "$serial_number" '.care_ai.hardware.nvidia.serial = $serial' .care.ai.id.json > temp.json && mv temp.json .care.ai.id.json
jq --arg wifiMAC "$wifi_mac" '.care_ai.hardware.wifi_module.MAC = $wifiMAC' .care.ai.id.json > temp.json && mv temp.json .care.ai.id.json
jq --arg ethMAC "$eth_mac" '.care_ai.hardware.ethernet_module.MAC = $ethMAC' .care.ai.id.json > temp.json && mv temp.json .care.ai.id.json
jq --arg tegREL "$tegra_rel" '.care_ai.firmware.nv_tegra_release = $tegREL' .care.ai.id.json > temp.json && mv temp.json .care.ai.id.json
jq --arg flashTS "$timestamp" '.care_ai.firmware.flash_timestamp = $flashTS' .care.ai.id.json > temp.json && mv temp.json .care.ai.id.json
rm ~/set_id.sh
